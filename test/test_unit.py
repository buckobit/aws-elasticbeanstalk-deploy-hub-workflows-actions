import configparser
from contextlib import contextmanager
from copy import copy
import io
import os
import sys

import botocore.session
from botocore.stub import Stubber
import shutil
from unittest import mock, TestCase


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class ElasticbeanstalkDeployTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path
        test_dir = os.path.join(os.environ["HOME"], '.aws')
        if os.path.exists(test_dir):
            shutil.rmtree(test_dir)

    @mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region', 'AWS_OIDC_ROLE_ARN': '',
                                  'ENVIRONMENT_NAME': 'test', 'APPLICATION_NAME': 'test', 'S3_BUCKET': 'test',
                                  'VERSION_LABEL': 'test-default-12345', 'ZIP_FILE': 'code/app.js'})
    def test_discover_auth_method_default_credentials_only(self):
        from pipe import pipe

        elasticbeanstalk_pipe = pipe.ElasticbeanstalkPipe(schema=pipe.schema,
                                                          check_for_newer_version=True)
        elasticbeanstalk_pipe.auth()
        self.assertEqual(elasticbeanstalk_pipe.auth_method, 'DEFAULT_AUTH')
        base_config_dir = f'{os.environ["HOME"]}/.aws'

        self.assertFalse(os.path.exists(f'{base_config_dir}/.aws-oidc'))

    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'ENVIRONMENT_NAME': 'test', 'APPLICATION_NAME': 'test', 'S3_BUCKET': 'test',
                                  'VERSION_LABEL': 'test-default-12345', 'ZIP_FILE': 'code/app.js'})
    def test_discover_auth_method_oidc_only(self):
        from pipe import pipe

        elasticbeanstalk_pipe = pipe.ElasticbeanstalkPipe(schema=pipe.schema,
                                                          check_for_newer_version=True)
        elasticbeanstalk_pipe.auth()
        self.assertEqual(elasticbeanstalk_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'ENVIRONMENT_NAME': 'test', 'APPLICATION_NAME': 'test', 'S3_BUCKET': 'test',
                                  'VERSION_LABEL': 'test-default-12345', 'ZIP_FILE': 'code/app.js'})
    def test_discover_auth_method_oidc_and_default_credentials(self):
        from pipe import pipe
        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)

        elasticbeanstalk_pipe = pipe.ElasticbeanstalkPipe(schema=pipe.schema,
                                                          check_for_newer_version=True)
        elasticbeanstalk_pipe.auth()
        self.assertEqual(elasticbeanstalk_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)

    @mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region', 'AWS_OIDC_ROLE_ARN': 'test',
                                  'ENVIRONMENT_NAME': 'test', 'APPLICATION_NAME': 'test', 'S3_BUCKET': 'test',
                                  'VERSION_LABEL': 'test-default-12345', 'ZIP_FILE': 'test/code/app.js',
                                  'WAIT': 'true', 'DESCRIPTION': 'description',
                                  'WAIT_INTERVAL': '2', 'WARMUP_INTERVAL': '2'})
    def test_success(self):
        from pipe import pipe
        # special mock for s3 since botocore does not support custom upload_file method
        s3 = mock.MagicMock()
        s3.return_value.upload_file.return_value = {}
        # stubber for elasticbeanstalk
        ebs = botocore.session.get_session().create_client('elasticbeanstalk')
        stubber = Stubber(ebs)
        # upload section
        stubber.add_response('create_application_version', {},
                             expected_params={
                                 'ApplicationName': os.getenv('APPLICATION_NAME'),
                                 'VersionLabel': os.getenv('VERSION_LABEL'),
                                 'Description': 'description',
                                 'SourceBundle':
                                     {'S3Bucket': os.getenv('S3_BUCKET'),
                                      'S3Key': f'{os.getenv("APPLICATION_NAME")}/{os.getenv("VERSION_LABEL")}.js'}})
        # deploy section
        stubber.add_response('update_environment',
                             {},
                             expected_params={'ApplicationName': os.getenv('APPLICATION_NAME'),
                                              'EnvironmentName': os.getenv('ENVIRONMENT_NAME'),
                                              'VersionLabel': os.getenv('VERSION_LABEL')}
                             )
        # wait for deploy section
        status_responses = [{'Environments': [{'Status': 'Updating', 'Health': 'Grey', 'EnvironmentId': '123',
                                               'CNAME': 'test-cname', 'VersionLabel': 'test-default-previous'}]},
                            {'Environments': [{'Status': 'Ready', 'Health': 'Red', 'EnvironmentId': '123',
                                               'VersionLabel': 'test-default-12345', 'CNAME': 'test-cname'}]},
                            {'Environments': [{'Status': 'Ready', 'Health': 'Green', 'EnvironmentId': '123',
                                               'VersionLabel': 'test-default-12345', 'CNAME': 'test-cname'
                                               }]}]
        for response in status_responses:
            stubber.add_response('describe_environments',
                                 response,
                                 expected_params={'ApplicationName': os.getenv('APPLICATION_NAME'),
                                                  'EnvironmentNames': [os.getenv('ENVIRONMENT_NAME')]}
                                 )
        stubber.activate()

        elasticbeanstalk_pipe = pipe.ElasticbeanstalkPipe(schema=pipe.schema,
                                                          check_for_newer_version=True)

        with capture_output() as out:
            with mock.patch.object(pipe.ElasticbeanstalkPipe, 'get_client', side_effect=[ebs, s3]):
                elasticbeanstalk_pipe.run()

        s3.upload_file.assert_called_once_with('test/code/app.js', 'test', 'test/test-default-12345.js')
        actual_output = out.getvalue()
        self.assertIn('✔ Artifact uploaded successfully to s3://test/test/test-default-12345', actual_output)
        self.assertIn('✔ Application version test-default-12345 successfully created in Elastic Beanstalk.',
                      actual_output)
        self.assertIn('✔ Deployment successful. URL: http://$test-cname', actual_output)
        self.assertIn('✔ Pipe finished successfully.', actual_output)
