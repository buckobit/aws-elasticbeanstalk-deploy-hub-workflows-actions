import logging
import os
import time
import stat
import configparser

import yaml
import boto3
from botocore.exceptions import ClientError

from bitbucket_pipes_toolkit import Pipe, get_logger


logger = get_logger()

schema = {
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': True},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': True},
    'AWS_DEFAULT_REGION': {'type': 'string', 'required': True},
    'APPLICATION_NAME': {'type': 'string', 'required': True},
    'COMMAND': {'type': 'string', 'required': True, 'default': 'all', 'allowed': ['all', 'upload-only', 'deploy-only']},
    'VERSION_LABEL': {
        'type': 'string', 'required': False,
        'default': f'{os.getenv("APPLICATION_NAME")}-'
                   f'{os.getenv("BITBUCKET_BUILD_NUMBER")}-'
                   f'{os.getenv("BITBUCKET_COMMIT", "local-run")[:8]}'},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}

upload_schema = {
    'ZIP_FILE': {'type': 'string', 'required': True},
    'S3_BUCKET': {'type': 'string',
                  'default': f'{os.getenv("APPLICATION_NAME")}-elasticbeanstalk-deployment'},
    'S3_KEY': {'type': 'string', 'required': False,
               'default': f'{os.getenv("APPLICATION_NAME")}/{os.getenv("VERSION_LABEL")}'},
    'DESCRIPTION': {'type': 'string', 'required': False,
                    'default':
                        f'Application version created from https://bitbucket.org/{os.getenv("BITBUCKET_REPO_OWNER")}/'
                        f'{os.getenv("BITBUCKET_REPO_SLUG")}/addon/pipelines/home#!/results/'
                        f'{os.getenv("BITBUCKET_BUILD_NUMBER")}'},
}


deploy_schema = {
    'ENVIRONMENT_NAME': {'type': 'string', 'required': True},
    'WAIT': {'type': 'boolean', 'required': False, 'default': False},
    'WAIT_INTERVAL': {'type': 'integer', 'required': False, 'default': 10},
    'WARMUP_INTERVAL': {'type': 'integer', 'required': False, 'default': 0},
}

ALLOWED_FILE_EXTENSIONS = ['zip', 'jar', 'war']


class ElasticbeanstalkPipe(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, *args, **kwargs):
        self.auth_method = self.discover_auth_method()
        super().__init__(*args, **kwargs)
        self.client = None

    def discover_auth_method(self):
        """Discover user intentions: authenticate to AWS through OIDC or default aws access keys"""
        oidc_role = os.getenv('AWS_OIDC_ROLE_ARN')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if web_identity_token:
                logger.info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')
                schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                schema['AWS_OIDC_ROLE_ARN'] = {'required': True}

                schema['AWS_ACCESS_KEY_ID']['required'] = False
                schema['AWS_SECRET_ACCESS_KEY']['required'] = False

                os.environ.pop('AWS_ACCESS_KEY_ID', None)
                os.environ.pop('AWS_SECRET_ACCESS_KEY', None)
                return self.OIDC_AUTH

            logger.warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')
            logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
            return self.DEFAULT_AUTH

        logger.info('Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
        return self.DEFAULT_AUTH

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            random_number = str(time.time_ns())
            aws_config_directory = os.path.join(os.environ["HOME"], '.aws')
            oidc_token_directory = os.path.join(aws_config_directory, '.aws-oidc')

            os.makedirs(aws_config_directory, exist_ok=True)
            os.makedirs(oidc_token_directory, exist_ok=True)

            web_identity_token_path = os.path.join(oidc_token_directory, f'oidc_token_{random_number}')
            with open(web_identity_token_path, 'w') as f:
                f.write(self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'))

            os.chmod(web_identity_token_path, mode=stat.S_IRUSR)
            logger.debug('Web identity token file is created')

            aws_configfile_path = os.path.join(aws_config_directory, 'config')
            with open(aws_configfile_path, 'w') as configfile:
                config = configparser.ConfigParser()
                config['default'] = {
                    'role_arn': self.get_variable('AWS_OIDC_ROLE_ARN'),
                    'web_identity_token_file': web_identity_token_path
                }
                config.write(configfile)
            logger.debug('Configured settings for authentication with assume web identity role')

    def get_client(self, module='elasticbeanstalk'):
        try:
            return boto3.client(module, region_name=self.get_variable('AWS_DEFAULT_REGION'))
        except ClientError as err:
            self.fail("Failed to create boto3 client.\n" + str(err))

    def get_environment_status(self, environment_details):
        self.log_debug(f'Getting environment status with details {environment_details}')
        try:
            return self.client.describe_environments(**environment_details)
        except ClientError as error:
            self.fail(f'Getting environment status failed with error: {error}')

    def upload(self):
        s3_bucket = self.get_variable('S3_BUCKET')
        s3_key = self.get_variable('S3_KEY')

        zip_file = self.get_variable('ZIP_FILE')
        extension = os.path.splitext(zip_file)[-1]
        extension_to_check = extension.strip('.')
        if extension_to_check not in ALLOWED_FILE_EXTENSIONS:
            self.log_warning("The application source bundle doesn\'t have a known file extension (zip, jar or war). "
                             "This might cause some issues.")

        s3 = self.get_client('s3')
        self.log_info(f'Uploading to s3 bucket: {s3_bucket}')
        source_bundle_key = f'{s3_key}{extension}'
        try:
            s3.upload_file(zip_file, s3_bucket, source_bundle_key)
        except ClientError as error:
            self.fail(f'Uploading file {zip_file} to bucket {s3_bucket} failed with error: {error}')

        self.success(f'Artifact uploaded successfully to s3://{s3_bucket}/{s3_key}')

        self.log_info('Creating application version in Elastic Beanstalk...')
        application_name = self.get_variable('APPLICATION_NAME')
        version_label = self.get_variable('VERSION_LABEL')
        description = self.get_variable('DESCRIPTION')
        try:
            self.client.create_application_version(ApplicationName=application_name,
                                                   VersionLabel=version_label,
                                                   Description=description,
                                                   SourceBundle={'S3Bucket': s3_bucket,
                                                                 'S3Key': source_bundle_key})
        except ClientError as error:
            self.fail(f'Creating application version {version_label} failed with error: {error}')

        self.success(f'Application version {version_label} successfully created in Elastic Beanstalk.')

    def deploy(self):
        application_name = self.get_variable('APPLICATION_NAME')
        environment_name = self.get_variable('ENVIRONMENT_NAME')
        version_label = self.get_variable('VERSION_LABEL')
        try:
            self.client.update_environment(ApplicationName=application_name,
                                           EnvironmentName=environment_name,
                                           VersionLabel=version_label)
        except ClientError as error:
            self.fail(f'Update environment {environment_name} with {application_name} application of version label {version_label} '
                      f'failed with error: {error}')

        environments = self.get_environment_status({'ApplicationName': application_name,
                                                    'EnvironmentNames': [environment_name, ]})

        environment_id = environments['Environments'][0]['EnvironmentId']
        url = environments['Environments'][0]['CNAME']
        version = environments['Environments'][0]['VersionLabel']
        default_region = self.get_variable('AWS_DEFAULT_REGION')
        self.log_info(f'Deploying to environment {environment_name}. '
                      f'Previous version: {version} -> New version: {version_label}.')
        self.log_info(f'Deployment triggered successfully. URL: http://${url}.')
        self.log_info(f'You can follow your deployment at '
                      f'https://console.aws.amazon.com/elasticbeanstalk/home?region={default_region}#/environment/'
                      f'dashboard?applicationName={application_name}&environmentId={environment_id}')
        return environments

    def wait_for_deploy(self, deploying_environment):
        attempt = 1
        application_name = self.get_variable('APPLICATION_NAME')
        environment_name = self.get_variable('ENVIRONMENT_NAME')
        status = deploying_environment['Environments'][0]['Status']
        while status in ('Launching', 'Updating'):
            time.sleep(self.get_variable('WAIT_INTERVAL'))
            self.log_info(f'Checking deployment status in Elastic Beanstalk. Attempt {attempt}')
            environments = self.get_environment_status({'ApplicationName': application_name,
                                                       'EnvironmentNames': [environment_name, ]})
            status = environments['Environments'][0]['Status']
            attempt += 1

        # final health check
        environments = self.get_environment_status({'ApplicationName': application_name,
                                                    'EnvironmentNames': [environment_name, ]})
        health = environments['Environments'][0]['Health']
        environment_version = environments['Environments'][0]['VersionLabel']
        target_version_label = self.get_variable('VERSION_LABEL')

        if environment_version != target_version_label:
            self.fail(f'Deployment failed. Environment {environment_name} '
                      f'is running a different version {environment_version}.')

        # add warmup time for some of customer edgecases
        warmup_interval = self.get_variable('WARMUP_INTERVAL')
        if health not in ('Green', 'Yellow'):
            time.sleep(warmup_interval)
            environments = self.get_environment_status({'ApplicationName': application_name,
                                                        'EnvironmentNames': [environment_name, ]})
            health = environments['Environments'][0]['Health']

        if health not in ('Green', 'Yellow'):
            self.fail(f'Deployment failed. Environment {environment_name} is {health}.')

        environment_url = environments['Environments'][0]['CNAME']
        status = environments['Environments'][0]['Status']
        self.log_info(f'Environment {environment_name} is now running version {environment_version} '
                      f'with status {status}. Environment health is {health}.')
        self.success(f'Deployment successful. URL: http://${environment_url}.')

    def run(self):
        super().run()

        # Logs
        self.log_info('Executing the aws-elasticbeanstalk-deploy pipe...')
        boto3.set_stream_logger('boto3.resources', logging.INFO)
        if self.get_variable('DEBUG'):
            boto3.set_stream_logger('boto3.resources', logging.DEBUG)

        # Main functionality
        self.auth()
        self.client = self.get_client()

        command = self.get_variable('COMMAND')
        if command == 'upload-only' or command == 'all':
            self.schema.update(upload_schema)
            variables = self.validate()
            self.variables.update(variables)
            self.upload()

        if command == 'deploy-only' or command == 'all':
            self.schema.update(deploy_schema)
            variables = self.validate()
            self.variables.update(variables)
            deploying_environment = self.deploy()
            if self.get_variable('WAIT'):
                self.wait_for_deploy(deploying_environment)

        self.success(
            message="Pipe finished successfully.")


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = ElasticbeanstalkPipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
